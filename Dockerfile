# analysis base version from which we start
FROM atlas/analysisbase:21.2.51

# Setup user and permissions
RUN adduser atlas -u 500 --disabled-password --gecos ""
RUN usermod -a -G 0 atlas
USER atlas

# put the code into the repo with the proper owner
COPY --chown=atlas . /code/src

# note that the build directory already exists in /code/src
RUN sudo chown -R atlas /code/src && \
    cd /code/src && \
    source /home/atlas/release_setup.sh && \  
    g++ `root-config --glibs --cflags` -o analysis.exe analysis.cpp
    
# where you are left after booting the image
WORKDIR /code/src
