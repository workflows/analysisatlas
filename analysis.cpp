#include<iostream>
#include <string>

#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"



int main(int argc, char *argv[]){

  std::cout<<"Running : "<<argc<<std::endl;

  if(argc<=1){
    std::cout<<"Not enough arguments"<<std::endl;
    return 3;
  }
  
  TH1F *h = new TH1F("h","h",10,0,10);
  
  for(int iarg=1; iarg<argc; iarg++){
  
    std::string::size_type sz;   // alias of size_t

    int argval = std::stoi (argv[iarg],&sz);
  
    std::cout<<"Arg : "<<iarg<<"  "<<argval<<std::endl;
    
    h->Fill(argval);
  }
  
  TFile *fout = new TFile("output.root","RECREATE");
  
  h->Write("myHist");
  
  fout->Close();
  
  TCanvas *c = new TCanvas("c","c",500,500);
  h->Draw("hist");
  c->SaveAs("plot.pdf");

  return 1;
}